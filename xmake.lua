set_xmakever("2.8.6")

-- used to load modules when calling 'import'
add_moduledirs("$(projectdir)/ext")

if is_host("windows") then
    if "$(env LUA_DIR)" == "" then
        if "$(env LUA_PATH)" ~= "" then
            set_runenv("LUA_DIR", "$(env LUA_PATH)")
        else
            xmake.error("Define LUA_DIR to point to Lua directory!")
        end
    end
end

--set_version("0.1.0")

add_requires("pkg-config", {system = true})
add_requires("glfw")
add_requires("opengl")
add_requires("imgui", {configs = { glfw_opengl3 = true }})
add_requires("implot")
add_requires("doctest")

-- should also add for CXX,ARMClang,AppleClang,LCC?
add_cxxflags("gcc::-std=c++17")
add_cxxflags("gcc::-Wall -Wextra -Wformat=2 -Wunused")
add_cxxflags("clang::-std=c++17")
add_cxxflags("clang::-Wall -Wextra -Wshadow -Wformat=2 -Wunused")
add_cxxflags("msvc::-std:c++17")
add_cxxflags("msvc::-W3 -O2 -Zc:__cplusplus")

if is_mode("debug") then
    add_defines("DEBUG")
    set_config("buildir", "build/debug")
else
    set_config("buildir", "build/release")
end
set_targetdir("$(buildir)")

-- main hekate library
target("owl")
set_kind("binary")
-- source files
add_files("app/main.cpp")
add_files("src/*.cpp")
add_includedirs("src")
-- link to
add_packages("imgui", { public = true })
add_packages("implot", { public = true })
add_packages("opengl", { public = true })
add_packages("glfw", { public = true })


-- benchmark app
target("benchmark")
set_kind("binary")
-- source files
add_files("src/*.cpp")
add_files("bench/benchmark.cpp")
add_includedirs("src")
-- link to


-- test app
target("utest")
set_kind("binary")
-- source files
add_files("src/*.cpp")
add_files("utest/*.cpp")
add_includedirs("src")
add_includedirs("utest")
-- link to
add_packages("doctest")
