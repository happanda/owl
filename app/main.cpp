#include <cmath>
#include <fstream>
#include <iostream>
#include <set>
#include <string>
#include "perlin.h"
#include "perlin2d.h"
#include "random.h"
#include "skip_list.h"
#include "math/ind.h"
#include "math/vec.h"
#include "rand/random_walk.h"

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>
#include <implot.h>

#include <GLFW/glfw3.h>

static void glfw_error_callback(int error, const char* description)
{
    fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

owl::rnd::random<float> sRand;

template <class T, class RandomGen>
void print(owl::skip_list<T, RandomGen> const& slist)
{
    for (auto it = std::cbegin(slist); it != std::cend(slist); ++it)
    {
        std::cout << *it << std::endl;
    }
}


int main(int argc, char* argv[])
{
    int const NumCurves = 5;
    int const N = 8;
    int const NumPoints = 150;
    float gradChanger = 0.0;

    float xs[NumCurves][NumPoints];
    float ys[NumCurves][NumPoints];

//    for (int k = 0; k < NumCurves; ++k)
//    {
//        //owl::rnd::perlin_rand_gradient<float, std::uint32_t> prd{ GradNum };
//        owl::rnd::perlin<float> prln{ 32 };
//
//        for (int i = 0; i < NumPoints; ++i)
//        {
//            xs[k][i] = (float)i / NumPoints;
//            ys[k][i] = prln((float)i / NumPoints);
//        }
//    }

	for (int k = 0; k < NumCurves; ++k) {
		owl::rnd::random_walk<float> rwalk{ 0.1f };
		float t0 = 0.f;
		xs[k][0] = t0;
		ys[k][0] = 5.f;
		for (int i = 1; i < NumPoints; ++i) {
			xs[k][i] = xs[k][i - 1] + 0.1f;
			ys[k][i] = rwalk(ys[k][i - 1], xs[k][i - 1], xs[k][i]);
			rwalk.change_deviation(std::sin(static_cast<float>(i) / NumPoints));
		}
	}

    float heatmap[NumPoints * NumPoints];
    owl::rnd::perlin_rand_gradient2d<float, std::uint32_t> prd{ N, N };
    owl::rnd::perlin2d<float> p2d{ N, N };


    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 1;
    const char* glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    GLFWwindow* window = glfwCreateWindow(1280, 720, "Dear ImGui GLFW+OpenGL3 example", NULL, NULL);
    if (window == NULL)
        return 1;
    glfwMakeContextCurrent(window);
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImPlot::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    // Setup Dear ImGui style
    ImGui::StyleColorsDark();

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);
    ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);

    while (!glfwWindowShouldClose(window))
    {
        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        glfwPollEvents();

        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        //ImPlot::ShowDemoWindow();
        //ImGui::ShowDemoWindow(nullptr);
        if (ImPlot::BeginPlot("Perlin 1D"))
        {
            for (int k = 0; k < NumCurves; ++k)
            {
                ImPlot::PlotLine(std::to_string(k).c_str(), xs[k], ys[k], NumPoints);
            }
            ImPlot::EndPlot();
        }

        for (int i = 0; i < N; ++i)
        {
            for (int j = 0; j < N; ++j)
            {
                p2d.gradient().updateGradient(i, j, gradChanger);
            }
        }
        for (int i = 0; i < NumPoints; ++i)
        {
            for (int j = 0; j < NumPoints; ++j)
            {
                heatmap[i + j * NumPoints] = p2d({ (float)i / NumPoints, (float)j / NumPoints });
            }
        }


        ImPlot::BustColorCache("Perlin 2D");
        ImPlot::PushColormap(ImPlotColormap_Viridis);
        if (ImPlot::BeginPlot("Perlin 2D", "", "", ImVec2(600, 600)))
        {
            ImGui::DragFloat("Seed", &gradChanger, 0.005f, 0.0f, 3.14f);
            ImPlot::PlotHeatmap("", heatmap, NumPoints, NumPoints, 0.0, 0.0, nullptr);
            ImPlot::EndPlot();
        }
        ImPlot::PopColormap();

        // Rendering
        ImGui::Render();
        int display_w, display_h;
        glfwGetFramebufferSize(window, &display_w, &display_h);
        glViewport(0, 0, display_w, display_h);
        glClearColor(clear_color.x * clear_color.w, clear_color.y * clear_color.w, clear_color.z * clear_color.w, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(window);
    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImPlot::DestroyContext();
    ImGui::DestroyContext();

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
