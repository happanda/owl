#pragma once


namespace owl::math
{
	template <class IntNumber>
	struct index2
	{
		static std::enable_if_t<
			std::is_integral<IntNumber>::value, bool> const _only_integer = true;

		IntNumber x;
		IntNumber y;
	};


	template <class IntNumber>
	auto operator-(index2<IntNumber> const& v)
	{
		return index2{ -v.x, -v.y };
	}

	template <class IntNumber>
	auto operator+(index2<IntNumber> const& lhs, index2<IntNumber> const& rhs)
	{
		return index2{ lhs.x + rhs.x, lhs.y + rhs.y };
	}

	template <class IntNumber>
	auto operator-(index2<IntNumber> const& lhs, index2<IntNumber> const& rhs)
	{
		return index2{ lhs.x - rhs.x, lhs.y - rhs.y };
	}

	template <class IntNumber>
	auto operator*(index2<IntNumber> const& lhs, index2<IntNumber> const& rhs)
	{
		return index2{ lhs.x * rhs.x, lhs.y * rhs.y };
	}

	template <class IntNumber>
	auto operator/(index2<IntNumber> const& lhs, index2<IntNumber> const& rhs)
	{
		return index2{ lhs.x / rhs.x, lhs.y / rhs.y };
	}


	template <class IntNumber, class Integer>
	auto operator+(index2<IntNumber> const& lhs, Integer const& scalar)
	{
		return index2<IntNumber>{ lhs.x + scalar, lhs.y + scalar };
	}

	template <class IntNumber, class Integer>
	auto operator-(index2<IntNumber> const& lhs, Integer const& scalar)
	{
		return index2<IntNumber>{ lhs.x - scalar, lhs.y - scalar };
	}

	template <class IntNumber, class Integer>
	auto operator*(index2<IntNumber> const& lhs, Integer const& scalar)
	{
		return index2<IntNumber>{ lhs.x * scalar, lhs.y * scalar };
	}

	template <class IntNumber, class Integer>
	auto operator/(index2<IntNumber> const& lhs, Integer const& scalar)
	{
		return index2<IntNumber>{ lhs.x / scalar, lhs.y / scalar };
	}


	template <class IntNumber>
	auto operator==(index2<IntNumber> const& lhs, index2<IntNumber> const& rhs)
	{
		return lhs.x == rhs.x && lhs.y == rhs.y;
	}

	template <class IntNumber>
	auto operator!=(index2<IntNumber> const& lhs, index2<IntNumber> const& rhs)
	{
		return lhs.x != rhs.x || lhs.y != rhs.y;
	}


	template <class IntNumber>
	auto const& operator+=(index2<IntNumber>& lhs, index2<IntNumber> const& rhs)
	{
		lhs.x += rhs.x;
		lhs.y += rhs.y;
		return lhs;
	}

	template <class IntNumber>
	auto const& operator-=(index2<IntNumber>& lhs, index2<IntNumber> const& rhs)
	{
		lhs.x -= rhs.x;
		lhs.y -= rhs.y;
		return lhs;
	}

	template <class IntNumber>
	auto const& operator*=(index2<IntNumber>& lhs, index2<IntNumber> const& rhs)
	{
		lhs.x *= rhs.x;
		lhs.y *= rhs.y;
		return lhs;
	}

	template <class IntNumber>
	auto const& operator/=(index2<IntNumber>& lhs, index2<IntNumber> const& rhs)
	{
		lhs.x /= rhs.x;
		lhs.y /= rhs.y;
		return lhs;
	}
}
