#pragma once
#include <type_traits>


namespace owl::math
{
	template <class RealNumber>
	struct vec2
	{
		static std::enable_if_t<
			std::is_floating_point<RealNumber>::value, bool> const _only_float = true;

		RealNumber x;
		RealNumber y;
	};

	template <class RealNumber>
	auto operator-(vec2<RealNumber> const& v)
	{
		return vec2<RealNumber>{ -v.x, -v.y };
	}

	template <class RealNumber>
	auto operator+(vec2<RealNumber> const& lhs, vec2<RealNumber> const& rhs)
	{
		return vec2<RealNumber>{ lhs.x + rhs.x, lhs.y + rhs.y };
	}

	template <class RealNumber>
	auto operator-(vec2<RealNumber> const& lhs, vec2<RealNumber> const& rhs)
	{
		return vec2<RealNumber>{ lhs.x - rhs.x, lhs.y - rhs.y };
	}

	template <class RealNumber>
	auto operator*(vec2<RealNumber> const& lhs, vec2<RealNumber> const& rhs)
	{
		return vec2<RealNumber>{ lhs.x * rhs.x, lhs.y * rhs.y };
	}

	template <class RealNumber>
	auto operator/(vec2<RealNumber> const& lhs, vec2<RealNumber> const& rhs)
	{
		return vec2<RealNumber>{ lhs.x / rhs.x, lhs.y / rhs.y };
	}

	template <class RealNumber>
	auto operator==(vec2<RealNumber> const& lhs, vec2<RealNumber> const& rhs)
	{
		return lhs.x == rhs.x && lhs.y == rhs.y;
	}

	template <class RealNumber>
	auto operator!=(vec2<RealNumber> const& lhs, vec2<RealNumber> const& rhs)
	{
		return lhs.x != rhs.x || lhs.y != rhs.y;
	}



	template <class RealNumber>
	auto operator+(vec2<RealNumber> const& lhs, RealNumber const& scalar)
	{
		return vec2<RealNumber>{ lhs.x + scalar, lhs.y + scalar };
	}

	template <class RealNumber>
	auto operator-(vec2<RealNumber> const& lhs, RealNumber const& scalar)
	{
		return vec2<RealNumber>{ lhs.x - scalar, lhs.y - scalar };
	}

	template <class RealNumber>
	auto operator*(vec2<RealNumber> const& lhs, RealNumber const& scalar)
	{
		return vec2<RealNumber>{ lhs.x * scalar, lhs.y * scalar };
	}

	template <class RealNumber>
	auto operator/(vec2<RealNumber> const& lhs, RealNumber const& scalar)
	{
		return vec2<RealNumber>{ lhs.x / scalar, lhs.y / scalar };
	}


	template <class RealNumber>
	auto const& operator+=(vec2<RealNumber>& lhs, vec2<RealNumber> const& rhs)
	{
		lhs.x += rhs.x;
		lhs.y += rhs.y;
		return lhs;
	}

	template <class RealNumber>
	auto const& operator-=(vec2<RealNumber>& lhs, vec2<RealNumber> const& rhs)
	{
		lhs.x -= rhs.x;
		lhs.y -= rhs.y;
		return lhs;
	}

	template <class RealNumber>
	auto const& operator*=(vec2<RealNumber>& lhs, vec2<RealNumber> const& rhs)
	{
		lhs.x *= rhs.x;
		lhs.y *= rhs.y;
		return lhs;
	}

	template <class RealNumber>
	auto const& operator/=(vec2<RealNumber>& lhs, vec2<RealNumber> const& rhs)
	{
		lhs.x /= rhs.x;
		lhs.y /= rhs.y;
		return lhs;
	}


	template <class RealNumber>
	auto dot(vec2<RealNumber> const& lhs, vec2<RealNumber> const& rhs)
	{
		return lhs.x * rhs.x + lhs.y * rhs.y;
	}
}

namespace std
{
	template<class RealNumber>
	owl::math::vec2<RealNumber> floor(owl::math::vec2<RealNumber> const& v)
	{
		return { std::floor(v.x), std::floor(v.y) };
	}

	template<class RealNumber>
	owl::math::vec2<RealNumber> abs(owl::math::vec2<RealNumber> const& v)
	{
		return { std::abs(v.x), std::abs(v.y) };
	}
} // namespace std
