#pragma once

#include <algorithm>
#include <cstdint>
#include <vector>
#include "random.h"
#include "step_functions.h"

namespace owl::rnd
{
	template<class RealNumber, class Size>
	class perlin_rand_gradient;


	template <class RealNumber = float,
		class GradientFunc = perlin_rand_gradient<RealNumber, std::uint32_t>>
	class perlin
	{
	public:
		using real_t = RealNumber;
		using size_t = std::uint32_t;
		using SmoothStepFunc = real_t(*)(real_t x);

	private:
		GradientFunc mGradient;
		SmoothStepFunc mSmoothStep;
		real_t mInverseInterval;

	public:
		perlin(size_t gradients = 32,
				SmoothStepFunc smoothstepfunc = smootherstep<real_t>)
			: mGradient(std::max({ gradients, size_t{2} }))
			, mSmoothStep(smoothstepfunc)
			, mInverseInterval(1)
		{
			mInverseInterval /= (mGradient.size() - 1);
		}

		template<typename TestClass = GradientFunc>
		perlin(GradientFunc gradientFunc,
				SmoothStepFunc smoothstepfunc = smootherstep<real_t>,
				typename std::enable_if<!std::numeric_limits<GradientFunc>::is_integer>::type* = 0)
			: mGradient(gradientFunc)
			, mSmoothStep(smoothstepfunc)
		{
			mInverseInterval = (real_t)1 / (mGradient.size() - 1);
		}

		real_t operator()(real_t atX) const
		{
			real_t atXscaled = atX * (mGradient.size() - 1);
			real_t atXscaledFloor = std::floor(atXscaled);

			size_t interval = (size_t)atXscaledFloor;
			if (interval == mGradient.size() - 1)
				return 0;

			real_t residue = (atXscaled - atXscaledFloor);

			real_t leftOffset = -residue;
			real_t rightOffset = leftOffset + 1;

			real_t leftProduct = leftOffset * mGradient(interval);
			real_t rightProduct = rightOffset * mGradient(interval + 1);

			real_t result = leftProduct + mSmoothStep(residue) * (rightProduct - leftProduct);
			return result;
		}

		size_t size() const
		{
			return mGradient.size();
		}
	};

	template<class RealNumber, class Size>
	class perlin_rand_gradient
	{
		random<RealNumber> mRand;
		std::vector<RealNumber> mGradients;

	public:
		perlin_rand_gradient(Size size)
			: mGradients(std::max({ size, Size{2} }))
		{
			for (RealNumber& num : mGradients)
			{
				num = mRand() * 2 - 1;
			}
		}

		RealNumber operator()(Size interval) const
		{
			return mGradients[interval];
		}

		Size size() const
		{
			return mGradients.size();
		}
	};
} // namespace owl
