#pragma once
#include <iostream>
#include <string_view>
#include <vector>

namespace owl
{
    template<class StringView>
    constexpr bool starts_with(StringView const& str, StringView const& prefix)
    {
        auto const str_len = str.size();
        auto const prefix_len = prefix.size();
        return std::equal(prefix.data(), prefix.data() + prefix_len,
            str.data(), str.data() + std::min(str_len, prefix_len));
    }

    template<class StringView>
    constexpr bool ends_with(StringView const& str, StringView const& suffix)
    {
        auto const str_len = str.size();
        auto const suffix_len = suffix.size();
        return std::equal(suffix.data(), suffix.data() + suffix_len,
            str.data() + str_len - std::min(str_len, suffix_len), str.data() + str_len);
    }

    /*
    template<class String, typename std::enable_if_t<
        std::is_same_v<typename String::value_type, char>, bool> = true>
    void print(String const& str)
    {
        std::cout << str;
    }

    template<class String, typename std::enable_if_t<
        std::is_same_v<typename String::value_type, wchar_t>, bool> = true>
    void print(String const& str)
    {
        std::wcout << str;
    }
    */

    namespace help
    {
        template<class CharT>
        auto compare_char_asc(CharT lhs, CharT rhs)
        {
            return lhs - rhs;
        }

        template<class CharT>
        auto compare_char_desc(CharT lhs, CharT rhs)
        {
            return rhs - lhs;
        }

        template<class String, class Comparer>
        auto max_suffix(String const& str, Comparer comp)
        {
            using char_type = typename String::value_type;
            using size_type = typename String::size_type;

            size_type const str_len = str.size();
            size_type   i = 0,
                        j = 1,
                        k = 1,
                        p = 1;
            while (j + k <= str_len)
            {
                char_type const a = str[j + k - 1];
                char_type const b = str[i + k - 1];
                auto const cmp = comp(a, b);
                if (cmp == 0)
                {
                    if (k == p)
                    {
                        j += p;
                        k = 1;
                    }
                    else
                    {
                        ++k;
                    }
                }
                else
                {
                    if (cmp < 0)
                    {
                        j += k;
                        p = j - i;
                    }
                    else
                    {
                        i = j++;
                        p = 1;
                    }
                    k = 1;
                } 
            }
            //print(str);
            //std::cout << ": " << i << " " << p << std::endl;
            return std::make_pair(i, p);
        }
    } // namespace help

    template<class String>
    auto match(String const& pattern, String const& str)
    {
        using char_type = typename String::value_type;
        using size_type = typename String::size_type;

        size_type const pat_len = pattern.length();
        size_type const str_len = str.length();

        std::pair<size_type, size_type> ms1 = help::max_suffix(pattern, help::compare_char_asc<char_type>);
        std::pair<size_type, size_type> ms2 = help::max_suffix(pattern, help::compare_char_desc<char_type>);

        //std::cout << pattern << std::endl;
        //std::cout << "L = " << ms1.first << ", P = " << ms1.second << std::endl;
        //std::cout << "L = " << ms2.first << ", P = " << ms2.second << std::endl;

        size_type L = ms1.first;
        size_type P = ms1.second;
        if (ms2.first > L)
        {
            L = ms2.first;
            P = ms2.second;
        }

        size_type pos = 0;

        if (L < pat_len / 2 && ends_with(std::string_view(pattern.data() + L, P),
                                         std::string_view(pattern.data(), L)))
        {
            //std::cout << "if" << std::endl;
            size_type s = 0;
            while (pos + pat_len <= str_len)
            {
                size_type i = std::max(L, s) + 1;
                while (i <= pat_len && pattern[i - 1] == str[pos + i - 1])
                {
                    ++i;
                }
                if (i <= pat_len)
                {
                    auto const p1 = (i >= L ? i - L : 0);
                    auto const p2 = (1 + s >= P ? 1 + s - P : 0);
                    pos += std::max(p1, p2);
                    s = 0;
                    //std::cout << "p1 = " << p1 << ", p2 = " << p2 << std::endl;
                    //std::cout << "pos = " << pos << std::endl;
                }
                else
                {
                    size_type j = L;
                    while (j > s && pattern[j - 1] == str[pos + j - 1])
                    {
                        --j;
                    }
                    if (j <= s)
                    {
                        return pos;
                    }
                    pos += P;
                    s = pat_len - P;
                    //std::cout << "p = " << P << std::endl;
                    //std::cout << "pos = " << pos << std::endl;
                    //std::cout << "s = " << s << std::endl;
                }
            }
        }
        else
        {
            size_type q = std::max(L, pat_len - L) + 1;
            //std::cout << "else" << std::endl;
            //std::cout << "q = " << q << std::endl;

            while (pos + pat_len <= str_len)
            {
                size_type i = L + 1;
                while (i <= pat_len && pattern[i - 1] == str[pos + i - 1])
                {
                    ++i;
                }
                if (i <= pat_len)
                {
                    pos = pos + i - L;
                    //std::cout << "pos += i - L = " << pos << std::endl;
                }
                else
                {
                    size_type j = L;
                    while (j > 0 && pattern[j - 1] == str[pos + j - 1])
                    {
                        --j;
                    }
                    if (j == 0)
                    {
                        return pos;
                    }
                    pos += q;
                    //std::cout << "pos += q = " << pos << std::endl;
                }
            }
        }
        return String::npos;
    }

    template <class String>
    auto find(String const& pattern, String const& str)
    {
        using size_type = typename String::size_type;
        using char_type = typename String::value_type;

        if (pattern.empty())
        {
            return static_cast<size_type>(0);
        }

        size_type const str_size = str.size();
        size_type const pat_size = pattern.size();
        char_type const* str_data = str.data();
        char_type const* pat_data = pattern.data();

        if (pat_size > str_size)
        {
            return String::npos;
        }

        std::vector<size_type> lps(pat_size, 0);
        for (size_type j = 1; j < pat_size; ++j)
        {
            if (*(pat_data + j) == *(pat_data + lps[j - 1]))
            {
                lps[j] = lps[j - 1] + 1;
            }
            else
            {
                size_type k = lps[j];
                while (k > 0 && *(pat_data + j) != *(pat_data + k))
                {
                    k = lps[k];
                }
                if (k > 0 || *(pat_data + j) == *(pat_data + k))
                {
                    lps[j] = lps[k] + 1;
                }
            }
        }

        size_type si { 0 },
                  pi { 0 };
        while (pi < pat_size && si < str_size)
        {
            if (*(pat_data + pi) == *(str_data + si))
            {
                ++si;
                ++pi;
            }
            else
            {
                if (pi == 0)
                {
                    ++si;
                }
                else
                {
                    pi = lps[pi - 1];
                }
            }
        }
        if (pi == pat_size)
        {
            return si - pi;
        }
        return String::npos;
    }

    template <class String>
    void reverse(String* str)
    {
        auto const size = str->size();
        auto* data = str->data();

        for (size_t i = 0; i < size / 2; ++i)
        {
            std::swap(*(data + i), *(data + size - i - 1));
        }
    }


} // namespace owl
