#pragma once

#include <chrono>
#include <random>

namespace owl::rnd
{
	template<typename RealNumber>
	class random_walk {
	public:
		random_walk(float deviation = 1,
			std::mt19937::result_type seed = std::chrono::high_resolution_clock::now().time_since_epoch().count())
			: mSigma(deviation)
			, mRandGen(seed) {
		}

		RealNumber operator()(RealNumber prevPosition, RealNumber prevTime, RealNumber time) {
			std::normal_distribution<RealNumber> normDist{ prevPosition, (time - prevTime) * mSigma };
			return normDist(mRandGen);
		}

		RealNumber operator()(RealNumber prevPosition, RealNumber deltaTime) {
			std::normal_distribution<RealNumber> normDist{ prevPosition, deltaTime * mSigma };
			return normDist(mRandGen);
		}

		void change_deviation(RealNumber deviation) {
			mSigma = deviation;
		}

	private:
		RealNumber mSigma{ 1 };
		std::mt19937 mRandGen;
		std::normal_distribution<RealNumber> mNormDist;
	};
} // namespace owl